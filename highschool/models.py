from django.db import models
from django.contrib.auth.models import User
from django.core.validators import RegexValidator, MaxValueValidator, MinValueValidator
from django.utils import timezone
from django.core.exceptions import ValidationError

import sys

class SexGenre(models.IntegerChoices):
    UNKNOWN = 0
    MALE = 1
    FEMALE = 2

class Subject(models.Model):
    """ This model is used to represent a specific subject. """

    # Non-relational model fields
    name = models.CharField(max_length=255)
    color = models.CharField(
        validators=[RegexValidator(
            regex=r'^#(?:[0-9a-fA-F]{1,2}){3}$',
            message='La couleur doit être écrite au format hexadécimal'
        )],
        max_length=7,
        null=False,
        default='#4287f5'
    )

    def __str__(self):
        return "%s" % self.name

class CourseHour(models.Model):
    """ This model is used to represent a specific course at a specific hour. """

    # Relational model fields
    classroom_subject = models.ForeignKey('ClassroomTeacher', on_delete=models.SET_NULL, null=True)

    # Non-relational model fields
    start_at = models.DateTimeField(default=timezone.datetime.today)
    end_at = models.DateTimeField(default=timezone.datetime.today)

class AcademicGrade(models.Model):
    """ This model is used to represent the academic grade. """

    # Non-relational model fields
    diploma_short_name = models.CharField(max_length=35)
    diploma_full_name = models.CharField(max_length=255)
    formation_short_name = models.CharField(max_length=255)
    formation_full_name = models.CharField(max_length=255)
    year_after_bac = models.IntegerField(help_text='Spécifiez le nombre d\'années avant ou après le BAC concernant le diplôme')

    def __str__(self):
        return "%s %s (BAC+%s)" % (self.diploma_short_name, self.formation_short_name, self.year_after_bac)

    def short_name(self):
        return "%s %s" % (self.diploma_short_name, self.formation_short_name)

    def long_name(self):
        return "%s %s" % (self.diploma_full_name, self.formation_full_name)


class Teacher(models.Model):
    """ This model represents a school teacher."""

    # Relational model fields
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    subject = models.ForeignKey('Subject', on_delete=models.SET_NULL, null=True, blank=True)

    # Non-relational model fields
    genre = models.IntegerField(choices=SexGenre.choices, default=SexGenre.UNKNOWN)
    birth_date = models.DateField(default=timezone.datetime.today)
    phone = models.CharField(
        validators=[RegexValidator(
            regex=r'^\+33[1-9][0-9]{8}$',
            message='Le numéro de téléphone doit suivre le format "+33125460945".'
        )],
        max_length=12,
        null=False,
        default='+33645712082'
    )

    def __str__(self):
        return "%s %s (%s)" % (
            self.user.first_name,
            self.user.last_name,
            self.subject.name if self.subject is not None else 'Aucun'
        )

    def full_name(self):
        return "%s %s" % (self.user.first_name, self.user.last_name)

    def delete(self, using=None, keep_parents=False):
        self.user.delete()
        return super(Teacher, self).delete(using, keep_parents)


class ClassroomTeacher(models.Model):
    """ This model represents the ManyToMany relationship between Classroom and Teacher. """

    # Relational model fields
    teacher = models.ForeignKey('Teacher', on_delete=models.CASCADE, null=True)
    classroom = models.ForeignKey('Classroom', on_delete=models.CASCADE, null=True)

    def __str__(self):
        return "%s / %s" % (self.teacher, self.classroom)

    def teacher_name(self):
        return self.teacher.full_name()

    def subject_name(self):
        return self.teacher.subject.name

    def course_name(self):
        return "%s" % self.teacher


class Classroom(models.Model):
    """ This model represents the classroom at a specific year. """

    # Relational model fields
    academic_grade = models.ForeignKey(AcademicGrade, on_delete=models.SET_NULL, null=True)
    teachers = models.ManyToManyField('Teacher', through='ClassroomTeacher')
    students = models.ManyToManyField('Student')

    # Non-relational model fields
    start_at = models.DateTimeField(default=timezone.datetime.today)
    end_at = models.DateTimeField(default=timezone.datetime.today)
    class_name = models.CharField(max_length=150, default="class_name")

    def __str__(self):
        return "%s (%s - %s)" % (self.class_name, self.start_at.year, self.end_at.year)

    def lessons(self):
        return [lesson for class_teacher in list(self.classroomteacher_set.all()) for lesson in list(class_teacher.lesson_set.all())]


class Attendance(models.Model):

    # Relational model fields
    lesson = models.ForeignKey('Lesson', on_delete=models.CASCADE, null=True)
    student = models.ForeignKey('Student', on_delete=models.CASCADE, null=True)

    # Non-relational model fields
    is_absent = models.BooleanField(default=False)

    def __str__(self):
        return "Appel du %s pour %s au cours %s" % (
            self.lesson.start_at.strftime('%d/%m/%Y à %H:%M:%S'),
            self.student,
            self.lesson.simple()
        )


class Student(models.Model):
    """ Students within the SMA are represented by this model. """

    # Relational model fields
    user = models.OneToOneField(to=User, on_delete=models.SET_NULL, null=True)

    # Non-relational model fields
    genre = models.IntegerField(choices=SexGenre.choices, default=SexGenre.UNKNOWN)
    birth_date = models.DateField(default=timezone.datetime.utcnow)
    phone = models.CharField(
        validators=[RegexValidator(
            regex=r'^\+33[1-9][0-9]{8}$',
            message='Le numéro de téléphone doit suivre le format "+33125460945".'
        )],
        max_length=12,
        null=False,
        default='+33645712082'
    )

    def __str__(self):
        return "%s %s" % (self.user.first_name, self.user.last_name)

    def delete(self, using=None, keep_parents=False):
        self.user.delete()
        super(Student, self).delete(using, keep_parents)

    def absences(self):
        return self.attendance_set.filter(is_absent=True)


class Grade(models.Model):
    """ This model represents the grade for a specific student. """

    # Relational model fields
    student = models.ForeignKey('Student', on_delete=models.CASCADE, null=True)
    course = models.ForeignKey('ClassroomTeacher', on_delete=models.CASCADE, null=True)

    # Non-relational model fields
    grade = models.IntegerField(
        validators=[
            MaxValueValidator(20),
            MinValueValidator(0)
        ]
    )
    coefficient = models.FloatField(
        validators=[
            MinValueValidator(0)
        ]
    )


class Lesson(models.Model):
    """ This model represents the lesson itself. """

    # Relational model fields
    classroom_teacher = models.ForeignKey(ClassroomTeacher, on_delete=models.CASCADE, null=True)

    # Non-relational model fields
    start_at = models.DateTimeField(default=timezone.datetime.today)
    end_at = models.DateTimeField(default=timezone.datetime.today)

    def __str__(self):
        return "%s avec %s pour la classe %s" % (
            self.classroom_teacher.teacher.subject,
            self.classroom_teacher.teacher.full_name(),
            self.classroom_teacher.classroom.class_name
        )

    def has_attendance(self):
        return self.attendance_set.count() > 0

    def absent_students(self):
        return [attendance.student for attendance in list(self.attendance_set.filter(is_absent=True))]

    def simple(self):
        return "Leçon n° %d, le %s de %s à %s" %(
            self.id,
            self.start_at.strftime('%d/%m/%Y'),
            self.start_at.strftime('%H:%M'),
            self.end_at.strftime('%H:%M')
        )

    def page_title(self):
        return "Leçon de &laquo; %s &raquo; du %s à %s" % (
            self.subject().name,
            self.start_at.strftime('%d/%m/%Y'),
            self.start_at.strftime('%H:%M'),
        )

    def teacher(self):
        return self.classroom_teacher.teacher

    def subject(self):
        return self.classroom_teacher.teacher.subject

    def subject_name(self):
        return self.subject().name

    def subject_color(self):
        return self.subject().color
