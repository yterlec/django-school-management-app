from django.views import generic
from django.urls import reverse_lazy
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserChangeForm
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

from ..models import Student
from ..forms import StudentForm, UserCreationForm


class StudentListView(LoginRequiredMixin, PermissionRequiredMixin, generic.ListView):
    """ List all existing students. """
    template_name = 'highschool/student/list.html'
    model = Student
    paginate_by = 25

    permission_required = (
        'highschool.view_student'
    )

class StudentCreateView(LoginRequiredMixin, PermissionRequiredMixin, generic.CreateView):
    """ Creates a new student. """
    model = Student
    form_class = StudentForm
    template_name = 'highschool/student/create_update.html'

    permission_required = (
        'highschool.create_student'
    )

    def get(self, request, *args, **kwargs):
        print(self.request.user.get_user_permissions())
        print(self.request.user.get_group_permissions())
        print(self.request.user.get_all_permissions())
        return super(StudentCreateView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(StudentCreateView, self).get_context_data(**kwargs)
        context['user_form'] = UserCreationForm()
        context['cs_form_type'] = 'create'
        return context

    def form_valid(self, form):
        user_form: UserCreationForm = UserCreationForm(data=self.request.POST)
        if user_form.is_valid() and form.is_valid():
            user: User = user_form.save()
            student: Student = form.save(commit=False)
            student.user_id = user.id
            student.save()
        return HttpResponseRedirect(reverse_lazy('highschool:student-list'))


class StudentUpdateView(LoginRequiredMixin, PermissionRequiredMixin, generic.UpdateView):
    """ Updates a specific student. """
    model = Student
    form_class = StudentForm
    template_name = 'highschool/student/create_update.html'

    permission_required = (
        'highschool.change_student'
    )

    def get_context_data(self, **kwargs):
        context = super(StudentUpdateView, self).get_context_data(**kwargs)
        context['user_form'] = UserChangeForm(self.request.POST or None, instance=self.object.user)
        context['cs_form_type'] = 'update'
        context['selected'] = {
            'genre': self.object.genre,
            'group_ids': [group.id for group in self.object.user.groups.all()],
            'permission_ids': [permission.id for permission in self.object.user.user_permissions.all()],
        }
        return context

    def form_valid(self, form):
        user_form: UserChangeForm = UserChangeForm(self.request.POST, instance=self.object.user)
        if user_form.is_valid():
            user: User = user_form.save()
            if form.is_valid():
                student: Student = form.save(commit=False)
                student.user_id = user.id
                student.save()
            else:
                return self.form_invalid(form)
        else:
            return self.form_invalid(user_form)
        return HttpResponseRedirect(reverse_lazy('highschool:student-detail', args=[self.object.id]))

    def get_success_url(self):
        return reverse_lazy('highschool:student-detail', args=[self.object.id])


class StudentDeleteView(LoginRequiredMixin, PermissionRequiredMixin, generic.DeleteView):
    model = Student
    template_name = 'highschool/student/delete.html'
    success_url = reverse_lazy('highschool:student-list')

    permission_required = (
        'highschool.delete_student'
    )

class StudentDetailView(LoginRequiredMixin, PermissionRequiredMixin, generic.DetailView):
    template_name = 'highschool/student/detail.html'
    model = Student

    permission_required = (
        'highschool.view_student'
    )