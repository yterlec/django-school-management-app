from typing import List

from django.views import generic
from django.urls import reverse_lazy
from django.utils.dateparse import parse_date
from django.shortcuts import redirect, render
from django.forms import modelformset_factory
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

import datetime

from ...models import Classroom, Attendance, Lesson
from ...forms import AttendanceForm

class ClassroomAttendanceListView(LoginRequiredMixin, PermissionRequiredMixin, generic.ListView):
    """ Lists all existing attendance for a specific classroom. """
    paginate_by = 25
    template_name = 'highschool/classroom/attendance/list.html'

    permission_required = (
        'highschool.view_attendance'
    )

    def get_queryset(self):
        queryset = Attendance.objects.values('id').filter(course__classroom_id=self.kwargs['pk'])
        # queryset = ClassroomTeacher.objects.filter(classroom_id=self.kwargs['pk']).order_by('classroom_id')
        return queryset

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(ClassroomAttendanceListView, self).get_context_data(object_list=object_list, **kwargs)
        context['classroom'] = Classroom.objects.get(pk=self.kwargs['pk'])
        return context


class LessonAttendanceCreateView(LoginRequiredMixin, PermissionRequiredMixin, generic.CreateView):
    form_class = AttendanceForm
    template_name = 'highschool/classroom/attendance/create_update.html'

    permission_required = (
        'highschool.create_attendance'
    )

    cs_formset_factory = None
    cs_formset_parts = {}

    def cs_setup_formset(self):
        """ Setup the required data for the formset management. """
        # Set-up formset parts
        self.cs_formset_parts['classroom'] = Classroom.objects.get(pk=self.kwargs['class_id'])
        self.cs_formset_parts['lesson'] = Lesson.objects.get(pk=self.kwargs['lesson_id'])

        self.cs_formset_parts['students'] = self.cs_formset_parts['classroom'].students.all()
        self.cs_formset_parts['initial'] = [{'student': student, 'lesson': self.cs_formset_parts['lesson']} for student in self.cs_formset_parts['students']]

        # Set-up formset factory
        self.cs_formset_factory = modelformset_factory(
            model=Attendance,
            form=AttendanceForm,
            can_delete=False,
            extra=len(self.cs_formset_parts['students']),
            min_num=len(self.cs_formset_parts['students']),
            max_num=len(self.cs_formset_parts['students'])
        )

    def get_context_data(self, **kwargs):
        self.cs_setup_formset()
        context = super(LessonAttendanceCreateView, self).get_context_data(**kwargs)
        context['cs_form_type'] = 'create'
        context['lesson'] = self.cs_formset_parts['lesson']
        context['classroom'] = self.cs_formset_parts['classroom']
        context['formset'] = self.cs_formset_factory(
            queryset=Attendance.objects.none(),
            initial=self.cs_formset_parts['initial']
        )
        return context

    def post(self, request, *args, **kwargs):
        self.cs_setup_formset()
        formset = self.cs_formset_factory(
            request.POST,
            queryset=Attendance.objects.none(),
            initial=self.cs_formset_parts['initial']
        )
        if formset.is_valid():
            return self.form_valid(formset)
        return render(
            self.request, self.template_name,
            {
                'formset': formset
            }
        )

    def form_valid(self, formset):
        for index, form in enumerate(formset.forms, start=0):
            attendance: Attendance = form.save(commit=False)
            attendance.save()
        return redirect(self.get_success_url())


    def get_success_url(self):
        return reverse_lazy('highschool:classroom-lesson-detail', args=[self.cs_formset_parts['classroom'].id, self.cs_formset_parts['lesson'].id])

class LessonAttendanceUpdateView(LoginRequiredMixin, PermissionRequiredMixin, generic.CreateView):
    form_class = AttendanceForm
    template_name = 'highschool/classroom/attendance/create_update.html'

    permission_required = (
        'highschool.change_attendance'
    )

    cs_formset_factory = None
    cs_formset_parts = {}

    def cs_setup_formset(self):
        """ Setup the required data for the formset management. """
        # Set-up formset parts
        self.cs_formset_parts['classroom'] = Classroom.objects.get(pk=self.kwargs['class_id'])
        self.cs_formset_parts['lesson'] = Lesson.objects.get(pk=self.kwargs['lesson_id'])

        self.cs_formset_parts['students'] = self.cs_formset_parts['classroom'].students.all()

        # Set-up formset factory
        self.cs_formset_factory = modelformset_factory(
            model=Attendance,
            form=AttendanceForm,
            can_delete=False,
            extra=len(self.cs_formset_parts['students']),
            min_num=len(self.cs_formset_parts['students']),
            max_num=len(self.cs_formset_parts['students'])
        )

    def get_context_data(self, **kwargs):
        self.cs_setup_formset()
        context = super(LessonAttendanceUpdateView, self).get_context_data(**kwargs)
        context['cs_form_type'] = 'update'
        context['lesson'] = self.cs_formset_parts['lesson']
        context['classroom'] = self.cs_formset_parts['classroom']
        context['formset'] = self.cs_formset_factory(
            queryset=Attendance.objects.filter(lesson_id=self.cs_formset_parts['lesson'].id),
        )
        return context

    def post(self, request, *args, **kwargs):
        self.cs_setup_formset()
        formset = self.cs_formset_factory(
            request.POST,
            queryset=Attendance.objects.filter(lesson_id=self.cs_formset_parts['lesson'].id),
        )
        if formset.is_valid():
            return self.form_valid(formset)
        return render(
            self.request, self.template_name,
            {
                'formset': formset,
                'cs_form_type': 'update',
                'lesson': self.cs_formset_parts['lesson'],
                'classroom': self.cs_formset_parts['classroom'],
            }
        )

    def form_valid(self, formset):
        for index, form in enumerate(formset.forms, start=0):
            attendance: Attendance = form.save(commit=False)
            attendance.save()
        return redirect(self.get_success_url())


    def get_success_url(self):
        print(self.cs_formset_parts)
        return reverse_lazy('highschool:classroom-lesson-detail', args=[self.cs_formset_parts['classroom'].id, self.cs_formset_parts['lesson'].id])