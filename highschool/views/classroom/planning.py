from django.views import generic
from django.urls import reverse_lazy
from django.utils.dateparse import parse_date
from django.shortcuts import redirect, render
from django.forms import modelformset_factory
from django.contrib.auth.mixins import LoginRequiredMixin

from ...models import Classroom

class ClassroomPlanning(LoginRequiredMixin, generic.TemplateView):
    template_name = 'highschool/classroom/planning/default.html'

    def get_context_data(self, **kwargs):
        context = super(ClassroomPlanning, self).get_context_data(**kwargs)
        classroom: Classroom = Classroom.objects.get(pk=self.kwargs['class_id'])
        print(classroom.classroomteacher_set.all())
        context['classroom'] = Classroom.objects.get(pk=self.kwargs['class_id'])
        return context