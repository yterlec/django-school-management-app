from typing import List

from django.views import generic
from django.urls import reverse_lazy
from django.utils.dateparse import parse_date
from django.shortcuts import redirect, render
from django.forms import modelformset_factory
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.core.exceptions import ValidationError

import datetime

from ...models import Classroom, Lesson
from ...forms import LessonForm

class ClassroomLessonListView(LoginRequiredMixin, PermissionRequiredMixin, generic.ListView):
    """ Lists all existing attendance for a specific classroom. """
    paginate_by = 25
    template_name = 'highschool/classroom/lesson/list.html'

    permission_required = (
        'highschool.view_lesson'
    )

    def get_queryset(self):
        classroom = Classroom.objects.get(pk=self.kwargs['class_id'])
        queryset = Lesson.objects.filter(classroom_teacher__classroom_id=classroom.id)
        return queryset

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(ClassroomLessonListView, self).get_context_data(object_list=object_list, **kwargs)
        context['classroom'] = Classroom.objects.get(pk=self.kwargs['class_id'])
        return context

class ClassroomLessonDetailView(LoginRequiredMixin, PermissionRequiredMixin, generic.DetailView):
    model = Lesson
    template_name = 'highschool/classroom/lesson/detail.html'

    permission_required = (
        'highschool.view_lesson'
    )

    def get_context_data(self, **kwargs):
        context = super(ClassroomLessonDetailView, self).get_context_data(**kwargs)
        context['classroom'] = Classroom.objects.get(pk=self.kwargs['class_id'])
        return context

class ClassroomLessonCreateView(LoginRequiredMixin, PermissionRequiredMixin, generic.CreateView):
    form_class = LessonForm
    template_name = 'highschool/classroom/lesson/create_update.html'

    permission_required = (
        'highschool.create_lesson'
    )

    def get_context_data(self, **kwargs):
        context = super(ClassroomLessonCreateView, self).get_context_data(**kwargs)
        context['cs_form_type'] = 'create'
        context['form'] = LessonForm(classroom_id=self.kwargs['class_id'])
        context['classroom'] = Classroom.objects.get(pk=self.kwargs['class_id'])
        return context

    def get_success_url(self):
        context = self.get_context_data()
        return reverse_lazy('highschool:classroom-lesson-list', args=[context['classroom'].id])

class ClassroomLessonUpdateView(LoginRequiredMixin, PermissionRequiredMixin, generic.UpdateView):
    model = Lesson
    form_class = LessonForm
    template_name = 'highschool/classroom/lesson/create_update.html'

    permission_required = (
        'highschool.change_lesson'
    )

    def get_context_data(self, **kwargs):
        context = super(ClassroomLessonUpdateView, self).get_context_data(**kwargs)
        context['cs_form_type'] = 'update'
        context['form'] = LessonForm(classroom_id=self.kwargs['class_id'], instance=self.object)
        context['classroom'] = Classroom.objects.get(pk=self.kwargs['class_id'])
        return context

    def get_success_url(self):
        context = self.get_context_data()
        return reverse_lazy('highschool:classroom-lesson-list', args=[context['classroom'].id])

class ClassroomLessonDeleteView(LoginRequiredMixin, PermissionRequiredMixin, generic.DeleteView):
    """ Deletes a specific classroom lesson. """
    model = Lesson
    template_name = 'highschool/classroom/lesson/delete.html'

    permission_required = (
        'highschool.delete_lesson'
    )

    def get_context_data(self, **kwargs):
        context = super(ClassroomLessonDeleteView, self).get_context_data(**kwargs)
        context['classroom'] = Classroom.objects.get(pk=self.kwargs['class_id'])
        return context

    def get_success_url(self):
        context = self.get_context_data()
        return reverse_lazy('highschool:classroom-lesson-list', args=[context['classroom'].id])