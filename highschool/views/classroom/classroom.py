from django.views import generic
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

from ...models import Classroom
from ...forms import ClassroomForm


class ClassroomListView(LoginRequiredMixin, generic.ListView):
    """ Lists all existing classrooms. """
    paginate_by = 25
    model = Classroom
    ordering = ['-academic_grade', 'start_at']
    template_name = 'highschool/classroom/list.html'

    permission_required = (
        'highschool.view_classroom'
    )


class ClassroomDetailView(LoginRequiredMixin, PermissionRequiredMixin, generic.DetailView):
    """ Details a specific classroom. """
    model = Classroom
    template_name = 'highschool/classroom/detail.html'

    permission_required = (
        'highschool.view_classroom'
    )


class ClassroomCreateView(LoginRequiredMixin, PermissionRequiredMixin, generic.CreateView):
    """ Creates a new classroom. """
    form_class = ClassroomForm
    template_name = 'highschool/classroom/create.html'
    success_url = reverse_lazy('highschool:classroom-list')

    permission_required = (
        'highschool.create_classroom'
    )

    def get_context_data(self, **kwargs):
        context = super(ClassroomCreateView, self).get_context_data(**kwargs)
        context['cs_form_type'] = 'create'
        return context



class ClassroomUpdateView(LoginRequiredMixin, PermissionRequiredMixin, generic.UpdateView):
    """ Updates a specific classroom. """
    model = Classroom
    form_class = ClassroomForm
    template_name = 'highschool/classroom/update.html'

    permission_required = (
        'highschool.change_classroom'
    )

    def get_context_data(self, **kwargs):
        context = super(ClassroomUpdateView, self).get_context_data(**kwargs)
        context['cs_form_type'] = 'update'
        context['selected'] = {
            'students': [student.id for student in self.object.students.all()],
            'academic_grade': self.object.academic_grade.id,
            'teachers': [course.id for course in self.object.teachers.all()]
        }
        return context

    def get_success_url(self):
        return reverse_lazy('highschool:classroom-detail', args=[self.object.id])


class ClassroomDeleteView(LoginRequiredMixin, PermissionRequiredMixin, generic.DeleteView):
    """ Deletes a specific classroom. """
    model = Classroom
    template_name = 'highschool/classroom/delete.html'
    success_url = reverse_lazy('highschool:classroom-list')

    permission_required = (
        'highschool.delete_classroom'
    )
