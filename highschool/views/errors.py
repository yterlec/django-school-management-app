from django.shortcuts import render

def handler400(request, exception):
    return render(request, 'highschool/errors/error_400.html', { 'exception': exception })

def handler403(request, exception):
    return render(request, 'highschool/errors/error_403.html', { 'exception': exception })

def handler404(request, exception):
    return render(request, 'highschool/errors/error_404.html', { 'exception': exception })

def handler500(request, exception):
    return render(request, 'highschool/errors/error_500.html', { 'exception': exception })