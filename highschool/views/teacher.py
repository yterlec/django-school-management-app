from django.views import generic
from django.urls import reverse_lazy
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserChangeForm
from django.http import HttpResponseRedirect
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

from ..models import Teacher
from ..forms import TeacherForm, UserCreationForm


class TeacherListView(LoginRequiredMixin, PermissionRequiredMixin, generic.ListView):
    """ List all existing teachers. """
    model = Teacher
    paginate_by = 25
    template_name = 'highschool/teacher/list.html'

    permission_required = (
        'highschool.view_teacher'
    )


class TeacherDetailView(LoginRequiredMixin, PermissionRequiredMixin, generic.DetailView):
    """ Details a specific teacher. """
    model = Teacher
    template_name = 'highschool/teacher/detail.html'

    permission_required = (
        'highschool.view_teacher'
    )


class TeacherCreateView(LoginRequiredMixin, PermissionRequiredMixin, generic.CreateView):
    """ Creates a new teacher. """
    model = Teacher
    form_class = TeacherForm
    template_name = 'highschool/teacher/create_update.html'

    permission_required = (
        'highschool.create_teacher'
    )

    def get_context_data(self, **kwargs):
        context = super(TeacherCreateView, self).get_context_data(**kwargs)
        context['user_form'] = UserCreationForm()
        context['cs_form_type'] = 'create'
        return context

    def form_valid(self, form):
        user_form: UserCreationForm = UserCreationForm(data=self.request.POST)
        if user_form.is_valid() and form.is_valid():
            user: User = user_form.save()
            teacher: Teacher = form.save(commit=False)
            teacher.user_id = user.id
            teacher.save()
        return HttpResponseRedirect(reverse_lazy('highschool:teacher-list'))


class TeacherUpdateView(LoginRequiredMixin, PermissionRequiredMixin, generic.UpdateView):
    """ Updates a specific student. """
    model = Teacher
    form_class = TeacherForm
    template_name = 'highschool/teacher/create_update.html'

    permission_required = (
        'highschool.change_teacher'
    )

    def get_context_data(self, **kwargs):
        context = super(TeacherUpdateView, self).get_context_data(**kwargs)
        context['user_form'] = UserChangeForm(self.request.POST or None, instance=self.object.user)
        context['cs_form_type'] = 'update'
        context['selected'] = {
            'genre': self.object.genre,
            'subject': self.object.subject.id if (self.object.subject is not None) else -1,
            'group_ids': [group.id for group in self.object.user.groups.all()],
            'permission_ids': [permission.id for permission in self.object.user.user_permissions.all()],
        }
        return context

    def form_valid(self, form):
        user_form: UserChangeForm = UserChangeForm(self.request.POST, instance=self.object.user)
        if user_form.is_valid():
            user: User = user_form.save()
            if form.is_valid():
                teacher: Teacher = form.save(commit=False)
                teacher.user_id = user.id
                teacher.save()
            else:
                return self.form_invalid(form)
        else:
            return self.form_invalid(user_form)
        return HttpResponseRedirect(reverse_lazy('highschool:teacher-detail', args=[self.object.id]))

    def get_success_url(self):
        return reverse_lazy('highschool:teacher-detail', args=[self.object.id])


class TeacherDeleteView(LoginRequiredMixin, PermissionRequiredMixin, generic.DeleteView):
    """ Deletes a specific teacher. """
    model = Teacher
    template_name = 'highschool/teacher/delete.html'
    success_url = reverse_lazy('highschool:teacher-list')

    permission_required = (
        'highschool.delete_teacher'
    )
