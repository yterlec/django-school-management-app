from django.views import generic
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

from ..models import AcademicGrade
from ..forms import AcademicGradeForm


class AcademicGradeListView(LoginRequiredMixin, PermissionRequiredMixin, generic.ListView):
    """ Lists all existing academic grades. """
    paginate_by = 25
    ordering = ['-id']
    model = AcademicGrade
    template_name = 'highschool/academic-grade/list.html'

    permission_required = (
        'highschool.view_academicgrade'
    )


class AcademicGradeDetailView(LoginRequiredMixin, PermissionRequiredMixin, generic.DetailView):
    """ Details a specific academic grade. """
    model = AcademicGrade
    template_name = 'highschool/academic-grade/detail.html'

    permission_required = (
        'highschool.view_academicgrade'
    )


class AcademicGradeCreateView(LoginRequiredMixin, PermissionRequiredMixin, generic.CreateView):
    """ Creates a new academic grade. """
    form_class = AcademicGradeForm
    success_url = reverse_lazy('highschool:academic-grade-list')
    template_name = 'highschool/academic-grade/create.html'

    permission_required = (
        'highschool.create_academicgrade'
    )

    def get_context_data(self, **kwargs):
        print(self.request.user.get_all_permissions())
        context = super(AcademicGradeCreateView, self).get_context_data(**kwargs)
        context['cs_form_type'] = 'create'
        return context


class AcademicGradeUpdateView(LoginRequiredMixin, PermissionRequiredMixin, generic.UpdateView):
    """ Updates a specific academic grade. """
    model = AcademicGrade
    form_class = AcademicGradeForm
    template_name = 'highschool/academic-grade/update.html'

    permission_required = (
        'highschool.change_academicgrade'
    )

    def get_context_data(self, **kwargs):
        context = super(AcademicGradeUpdateView, self).get_context_data(**kwargs)
        context['cs_form_type'] = 'update'
        return context

    def get_success_url(self):
        return reverse_lazy('highschool:academic-grade-detail', args=[self.object.id])


class AcademicGradeDeleteView(LoginRequiredMixin, PermissionRequiredMixin, generic.DeleteView):
    """ Deletes a specific academic grade. """
    model = AcademicGrade
    template_name = 'highschool/academic-grade/delete.html'
    success_url = reverse_lazy('highschool:academic-grade-list')

    permission_required = (
        'highschool.delete_academicgrade'
    )
