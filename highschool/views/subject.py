from django.views import generic
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin

from ..models import Subject


class SubjectListView(LoginRequiredMixin, PermissionRequiredMixin, generic.ListView):
    """ Lists all subjects. """
    model = Subject
    paginate_by = 25
    template_name = 'highschool/subject/list.html'
    ordering = ['-name']

    permission_required = (
        'highschool.view_subject'
    )


class SubjectDetailView(LoginRequiredMixin, PermissionRequiredMixin, generic.DetailView):
    """ Details a specific subject. """
    model = Subject
    template_name = 'highschool/subject/detail.html'

    permission_required = (
        'highschool.view_subject'
    )


class SubjectCreateView(LoginRequiredMixin, PermissionRequiredMixin, generic.CreateView):
    """ Creates a new subject. """
    model = Subject
    fields = ['name', 'color']
    success_url = reverse_lazy('highschool:subject-list')
    template_name = 'highschool/subject/create.html'

    def get_context_data(self, **kwargs):
        context = super(SubjectCreateView, self).get_context_data(**kwargs)
        context['cs_form_type'] = 'create'
        return context

    permission_required = (
        'highschool.create_subject'
    )


class SubjectUpdateView(LoginRequiredMixin, PermissionRequiredMixin, generic.UpdateView):
    """ Updates a specific subject. """
    model = Subject
    fields = ['name', 'color']
    template_name = 'highschool/subject/update.html'

    permission_required = (
        'highschool.change_subject'
    )

    def get_context_data(self, **kwargs):
        context = super(SubjectUpdateView, self).get_context_data(**kwargs)
        context['cs_form_type'] = 'update'
        return context

    def get_success_url(self):
        return reverse_lazy('highschool:subject-detail', args=[self.object.id])


class SubjectDeleteView(LoginRequiredMixin, PermissionRequiredMixin, generic.DeleteView):
    """ Deletes a specific subject. """
    model = Subject
    success_url = reverse_lazy('highschool:subject-list')
    template_name = 'highschool/subject/delete.html'

    permission_required = (
        'highschool.delete_subject'
    )
