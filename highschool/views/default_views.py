from django.shortcuts import render
from django.contrib.auth.decorators import login_required

import datetime

from ..models import AcademicGrade, Student, Teacher, Classroom


@login_required
def index(request):
    context = {
        'statistics': {
            'academic_grades': AcademicGrade.objects.count(),
            'students': Student.objects.count(),
            'teachers': Teacher.objects.count()
        },
        'academics_grades': AcademicGrade.objects.all()
    }
    return render(request, 'highschool/index.html', context)


