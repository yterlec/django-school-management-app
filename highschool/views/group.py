from django.views import generic
from django.urls import reverse_lazy
from django.contrib.auth.models import Group
from django.contrib.auth.mixins import LoginRequiredMixin

from ..forms import GroupForm


class GroupListView(LoginRequiredMixin, generic.ListView):
    """ List all existing groups. """
    model = Group
    paginate_by = 25
    ordering = ['-id']
    template_name = 'highschool/group/list.html'


class GroupDetailView(LoginRequiredMixin, generic.DetailView):
    """ Details a specific group. """
    model = Group
    template_name = 'highschool/group/detail.html'

class GroupCreateView(LoginRequiredMixin, generic.CreateView):
    """ Creates a new group. """
    form_class = GroupForm
    template_name = 'highschool/group/create.html'
    success_url = reverse_lazy('highschool:group-list')

    def get_context_data(self, **kwargs):
        context = super(GroupCreateView, self).get_context_data(**kwargs)
        context['cs_form_type'] = 'create'
        return context


class GroupUpdateView(LoginRequiredMixin, generic.UpdateView):
    """ Updates a specific group. """
    model = Group
    form_class = GroupForm
    template_name = 'highschool/group/update.html'

    def get_context_data(self, **kwargs):
        context = super(GroupUpdateView, self).get_context_data(**kwargs)
        context['cs_form_type'] = 'update'
        context['selected'] = {
            'permission_ids': [perm.id for perm in self.object.permissions.all()]
        }
        return context

    def get_success_url(self):
        return reverse_lazy('highschool:group-detail', args=[self.object.id])


class GroupDeleteView(LoginRequiredMixin, generic.DeleteView):
    """ Deletes a specific group. """
    model = Group
    success_url = reverse_lazy('highschool:group-list')
    template_name = 'highschool/group/delete.html'