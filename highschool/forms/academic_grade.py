from django import forms

from ..models import AcademicGrade


class AcademicGradeForm(forms.ModelForm):
    class Meta:
        model = AcademicGrade
        fields = ['diploma_short_name', 'diploma_full_name', 'formation_short_name', 'formation_full_name', 'year_after_bac']