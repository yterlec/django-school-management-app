from django import forms
from django.contrib.auth import models as auth_models
from django.contrib.auth.forms import UserCreationForm as OldUserCreationForm

class UserCreationForm(OldUserCreationForm):
    class Meta:
        model = auth_models.User
        fields = [
            'username', 'first_name', 'last_name',
            'email', 'password1', 'password2',
            'user_permissions', 'groups',
            'is_staff', 'is_superuser',
            'date_joined', 'is_active'
        ]