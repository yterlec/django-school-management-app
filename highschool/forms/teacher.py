from django import forms

from ..models import Teacher


class TeacherForm(forms.ModelForm):
    class Meta:
        model = Teacher
        fields = ['phone', 'birth_date', 'genre', 'subject']
        exclude= ['user']