from django import forms

from ..models import Classroom


class ClassroomForm(forms.ModelForm):
    class Meta:
        model = Classroom
        fields = ['academic_grade', 'teachers', 'students', 'start_at', 'end_at', 'class_name']