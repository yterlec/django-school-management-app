from django import forms

from ..models import Lesson, ClassroomTeacher


class LessonForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        classroom_id = None
        if 'classroom_id' in kwargs:
            classroom_id = kwargs.pop('classroom_id')

        super(LessonForm, self).__init__(*args, **kwargs)

        if classroom_id is not None:
            self.fields['classroom_teacher'].queryset = ClassroomTeacher.objects.all().filter(classroom_id=classroom_id)

    class Meta:
        model = Lesson
        fields = ['classroom_teacher', 'start_at', 'end_at']
        widgets = {
            'start_at': forms.DateTimeInput(
                format='%Y-%m-%dT%H:%M',
                attrs={
                    'type': 'datetime-local'
                }
            ),
            'end_at': forms.DateTimeInput(
                format='%Y-%m-%dT%H:%M',
                attrs={
                    'type': 'datetime-local'
                }
            ),
        }