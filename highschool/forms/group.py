from django import forms

from django.contrib.auth import models as auth_models

class GroupForm(forms.ModelForm):
    class Meta:
        model = auth_models.Group
        fields = ['name', 'permissions']