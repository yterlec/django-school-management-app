from django.urls import path, reverse_lazy
from django.views.generic import RedirectView
from . import views

app_name = 'highschool'
urlpatterns = [
    # example: /lycee/
    path('', views.index, name='index'),

    # Example (see all): /lycee/students
    path('students', views.StudentListView.as_view(), name='student-list'),
    # Example (see specific): /lycee/student/1
    path('student/<int:pk>', views.StudentDetailView.as_view(), name='student-detail'),
    # Example (create): /lycee/student/create
    path('student/create', views.StudentCreateView.as_view(), name='student-create'),
    # Example (edit): /lycee/student/edit/1
    path('student/update/<pk>', views.StudentUpdateView.as_view(), name='student-update'),
    # Example (delete): /lycee/student/delete/1
    path('student/delete/<pk>', views.StudentDeleteView.as_view(), name='student-delete'),

    #region -> (permissions)

    path('groups', views.GroupListView.as_view(), name='group-list'),
    path('group/<int:pk>', views.GroupDetailView.as_view(), name='group-detail'),
    path('group/create', views.GroupCreateView.as_view(), name='group-create'),
    path('group/update/<int:pk>', views.GroupUpdateView.as_view(), name='group-update'),
    path('group/delete/<int:pk>', views.GroupDeleteView.as_view(), name='group-delete'),

    # endregion

    # region -> (teachers)

    path('teachers', views.TeacherListView.as_view(), name='teacher-list'),
    path('teacher/<int:pk>', views.TeacherDetailView.as_view(), name='teacher-detail'),
    path('teacher/create', views.TeacherCreateView.as_view(), name='teacher-create'),
    path('teacher/update/<int:pk>', views.TeacherUpdateView.as_view(), name='teacher-update'),
    path('teacher/delete/<int:pk>', views.TeacherDeleteView.as_view(), name='teacher-delete'),

    # endregion

    # region -> (academic grades)

    path('academic-grades', views.AcademicGradeListView.as_view(), name='academic-grade-list'),
    path('<int:pk>', views.AcademicGradeDetailView.as_view(), name='academic-grade-detail'),
    path('academic-grade/create', views.AcademicGradeCreateView.as_view(), name='academic-grade-create'),
    path('academic-grade/update/<int:pk>', views.AcademicGradeUpdateView.as_view(), name='academic-grade-update'),
    path('academic-grade/delete/<int:pk>', views.AcademicGradeDeleteView.as_view(), name='academic-grade-delete'),

    # endregion

    # region -> (classroom)

    path('classrooms', views.ClassroomListView.as_view(), name='classroom-list'),
    path('classroom/<int:pk>', views.ClassroomDetailView.as_view(), name='classroom-detail'),
    path('classroom/create', views.ClassroomCreateView.as_view(), name='classroom-create'),
    path('classroom/update/<int:pk>', views.ClassroomUpdateView.as_view(), name='classroom-update'),
    path('classroom/delete/<int:pk>', views.ClassroomDeleteView.as_view(), name='classroom-delete'),

    # Classroom calls registers
    # path('classroom/<int:pk>/call_registers', views.ClassroomAttendanceListView.as_view(), name='classroom-call-register-list'),
    # path('classroom/<int:class_id>/call_register/<int:pk>', views.ClassroomAttendanceCreateView.as_view(), name='classroom-call-register-detail'),
    # path('classroom/<int:pk>/call_register/create', views.ClassroomAttendanceCreateView.as_view(), name='classroom_call_the_roll'),
    # path('classroom/<int:class_id>/call_register/update/<int:pk>', views.ClassroomAttendanceCreateView.as_view(), name='classroom-call-register-update'),
    # path('classroom/<int:class_id>/call_register/delete/<int:pk>', views.ClassroomAttendanceCreateView.as_view(), name='classroom-call-register-delete'),

    # Classroom planning
    path('classroom/<int:class_id>/planning', views.ClassroomPlanning.as_view(), name='classroom-planning'),

    # Classroom lessons management
    path('classroom/<int:class_id>/lessons', views.ClassroomLessonListView.as_view(), name='classroom-lesson-list'),
    path('classroom/<int:class_id>/lesson/<int:pk>', views.ClassroomLessonDetailView.as_view(), name='classroom-lesson-detail'),
    path('classroom/<int:class_id>/lesson/create', views.ClassroomLessonCreateView.as_view(), name='classroom-lesson-create'),
    path('classroom/<int:class_id>/lesson/update/<int:pk>', views.ClassroomLessonUpdateView.as_view(), name='classroom-lesson-update'),
    path('classroom/<int:class_id>/lesson/delete/<int:pk>', views.ClassroomLessonDeleteView.as_view(), name='classroom-lesson-delete'),

    # Classroom lesson attendance
    path('classroom/<int:class_id>/lesson/<int:lesson_id>/call_register/create', views.LessonAttendanceCreateView.as_view(), name='classroom-lesson-attendance-create'),
    path('classroom/<int:class_id>/lesson/<int:lesson_id>/call_register/update', views.LessonAttendanceUpdateView.as_view(), name='classroom-lesson-attendance-update'),


    # path('classroom/<int:class_id>/course/create', name='classroom-course-create'),

    # endregion

    # region -> (subject)

    path('subjects', views.SubjectListView.as_view(), name='subject-list'),
    path('subject/<int:pk>', views.SubjectDetailView.as_view(), name='subject-detail'),
    path('subject/create', views.SubjectCreateView.as_view(), name='subject-create'),
    path('subject/update/<int:pk>', views.SubjectUpdateView.as_view(), name='subject-update'),
    path('subject/delete/<int:pk>', views.SubjectDeleteView.as_view(), name='subject-delete'),

    # endregion
]
