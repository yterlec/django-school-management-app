class PermissionsRequiredMixin(object):
    permissions_required = None

    def check_user(self, user=None):
        final = []
        for required_permission in self.permissions_required:
            final.append(user.has_perm(required_permission))
        print(False in final)
        return False in final
