from django.urls import path
from django.contrib.auth import views as auth_views
from django.views.generic import RedirectView

from .views import LoginView

app_name = 'accounts'
urlpatterns = [
    path('', RedirectView.as_view(url='/accounts/login')),
    path('login', LoginView.as_view(), name='login'),
    path('logout', auth_views.LogoutView.as_view(), name='logout')
]
