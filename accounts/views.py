from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import TemplateView
from django.contrib.auth import views

class LoginView(views.LoginView):
    template_name = 'accounts/login.html'
    success_url = reverse_lazy('highschool:index')
